export const HamburgerIcon = ({
  width,
  height,
  stroke,
}: {
  width?: string;
  height?: string;
  stroke?: string;
}) => {
  return (
    <svg
      width="22"
      height="17"
      viewBox="0 0 22 17"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      style={{
        width: width ?? "22px",
        height: height ?? "17px",
      }}
    >
      <path
        d="M1.66675 15.1667H20.3334M1.66675 8.50001H20.3334M1.66675 1.83334H20.3334"
        stroke={stroke ?? "#00171F"}
        stroke-width="2"
        stroke-linecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
};
