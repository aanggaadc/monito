import { ButtonIcon, Button } from "../shared";
import { SearchIcon, StarIcon } from "../icons";

import Styles from "./right-content.module.scss";

export default function RightContent() {
  return (
    <div className={Styles.container}>
      <div className={Styles.inputSearch}>
        <ButtonIcon>
          <SearchIcon fill="#667479" width="20px" height="20px" />
        </ButtonIcon>
        <input type="text" placeholder="Search something here" />
      </div>

      <Button className={Styles.btnJoin}>Join the community</Button>

      <div className={Styles.selectContainer}>
        <StarIcon />
        <select>
          <option value="en">VND</option>
        </select>
      </div>
    </div>
  );
}
