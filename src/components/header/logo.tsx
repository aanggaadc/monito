import Styles from "./logo.module.scss";

export default function Logo() {
  return (
    <div className={Styles.logo}>
      <img src="/monito.svg" alt="monito" />

      <h1 className={Styles.hidden}>monito</h1>
    </div>
  );
}
