import Logo from "./logo";
import Menus from "./menus";
import RightContent from "./right-content";
import { ButtonIcon } from "../shared";
import { HamburgerIcon, SearchIcon } from "../icons";

import Styles from "./index.module.scss";

export default function Header() {
  return (
    <header className={Styles.header}>
      <div className={Styles.container}>
        <ButtonIcon className={Styles.btnMobile}>
          <HamburgerIcon />
        </ButtonIcon>

        <Logo />

        <Menus />

        <RightContent />

        <ButtonIcon className={Styles.btnMobile}>
          <SearchIcon />
        </ButtonIcon>
      </div>
    </header>
  );
}
