import clsx from "clsx";
import Styles from "./index.module.scss";

type ButtonProps = React.HTMLAttributes<HTMLButtonElement> & {
  children: React.ReactNode;
  className?: string;
};

export const ButtonIcon = (props: ButtonProps) => {
  const { className, ...rest } = props;

  return (
    <button className={clsx(Styles.button, className)} {...rest}>
      {props.children}
    </button>
  );
};
