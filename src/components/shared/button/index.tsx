import clsx from "clsx";
import Styles from "./index.module.scss";

type ButtonProps = React.HTMLAttributes<HTMLButtonElement> & {
  children: React.ReactNode;
  className?: string;
  variant?: "primary" | "secondary";
};

export const Button = (props: ButtonProps) => {
  const { className, variant = "primary", ...rest } = props;

  const variants = {
    primary: Styles.primary,
    secondary: Styles.secondary,
  };

  return (
    <button
      className={clsx(Styles.button, variants[variant], className)}
      {...rest}
    >
      {props.children}
    </button>
  );
};
