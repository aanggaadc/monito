import { Button } from "../shared";

import Styles from "./index.module.scss";

export default function Hero() {
  return (
    <div className={Styles.container}>
      <div className={Styles.element1}>
        <img src="/assets/images/hero/bg-element-1.png" alt="element" />
      </div>

      <div className={Styles.content}>
        <span>
          <h2 className={Styles.title}>One more friend</h2>
          <p className={Styles.subTitle}>Thousand More Fun!</p>
          <p className={Styles.description}>
            Having a pet means you have more joy, a new friend, a happy person
            who will always be with you to have fun. We have 200+ different pets
            that can meet your needs!
          </p>

          <div className={Styles.ctaContainer}>
            <Button variant="secondary">
              View Intro
              <img src="/assets/icons/play-circle.png" alt="play" />
            </Button>

            <Button>Explore Now</Button>
          </div>
        </span>

        <div className={Styles.bannerContainer}>
          <img src="/assets/images/hero/banner.png" alt="banner" />

          <img
            className={Styles.rectangle1}
            src="/assets/images/hero/rectangle-1.png"
            alt="rectangle"
          />
          <img
            className={Styles.rectangle2}
            src="/assets/images/hero/rectangle-2.png"
            alt="rectangle"
          />
        </div>
      </div>
    </div>
  );
}
