import Header from "./components/header";
import Hero from "./components/hero";

function App() {
  return (
    <>
      <Header />

      <main>
        <Hero />
      </main>
    </>
  );
}

export default App;
